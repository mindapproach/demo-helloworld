
def internalBuild(){
  echo "internalBuild() function begin"

  stage name: "build", concurrency: 1
  node{
    def mvnHome = tool 'M3.2'

    sh "${mvnHome}/bin/mvn clean"
		
		// ------------------------------------------------------------------------

    sh "${mvnHome}/bin/mvn pl.project13.maven:git-commit-id-plugin:revision"

    def strVersion       = getVersionStringFromPom()
    def majorVersion     = getMajorVersion( strVersion )
    def minorVersion     = getMinorVersion( strVersion )
    def strGitProperties = readFile( 'target/generated-resources/git.properties' )
    def commitTimestamp  = getCommitTimestamp( strGitProperties )
		def newVersion       = "$majorVersion.$minorVersion-$commitTimestamp"

    echo "strVersion     : $strVersion"
    echo "majorVersion   : $majorVersion"
    echo "minorVersion   : $minorVersion"
    echo "commitTimestamp: $commitTimestamp"
		echo "newVersion     : $newVersion"

    sh "${mvnHome}/bin/mvn versions:set -DnewVersion=${majorVersion}.${minorVersion}-${commitTimestamp}"

		// ------------------------------------------------------------------------
		
		echo "DOCKER_HOST     : ${env.DOCKER_HOST}"
		echo "DOCKER_CERT_PATH: ${env.DOCKER_CERT_PATH}"
		echo "DOCKER_REGISTRY : ${env.DOCKER_REGISTRY}"

    sh "${mvnHome}/bin/mvn package docker:build -U -DpushImage=true -PbuildDockerWorkDir -PbuildDockerImageWithSpotifyAndPushToPrivateRegistry"
  }

  echo "internalBuild() function end"
}

return this;

// ==========================================================================================================

def String getMajorVersion( String str )
{
  def matcher = str =~ '(\\d+)[.-](\\d+)[.-](.*)'
  matcher ? matcher[0][1] : null
}

def String getMinorVersion( String str )
{
  def matcher = str =~ '(\\d+)[.-](\\d+)[.-](.*)'
  matcher ? matcher[0][2] : null
}

def String getPatchVersion( String str )
{
  def matcher = str =~ '(\\d+)[.-](\\d+)[.-](.*)'
  matcher ? matcher[0][3] : null
}

def getVersionStringFromPom()
{
  def matcher = readFile( 'pom.xml' ) =~ '<version>(.+)</version>'
  matcher ? matcher[0][1] : null
}

def String getCommitTimestamp( String str )
{
  def matcher = str =~ 'git.commit.time=(.+)'
  matcher ? matcher[0][1] : null
}

def String getUrl( String str )
{
  def matcher = str =~ '.*://(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}).*'
  matcher ? matcher[0][1] : null
}
